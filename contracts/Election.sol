// SPDX-License-Identifier: MIT
pragma solidity >=0.4.25 <0.7.0;

contract Election {
    struct Candidate {
        uint id;
        string name;
        uint voteCount;
    }

    uint public totalCandidates = 0;
    mapping(uint => Candidate) public candidates;
    mapping(address => bool) public voters;

    event CandidateAdded(uint id, string name, uint voteCount);
    event Voted(uint id);

    constructor() public {
        addCandidate("Dummy");
    }

    function addCandidate(string memory _name) public {
        totalCandidates++;
        candidates[totalCandidates] = Candidate(totalCandidates, _name, 0);
        emit CandidateAdded(totalCandidates, _name, 0);
    }

    function vote(uint _id) public {
        // require that they haven't voted before
        require(!voters[msg.sender]);
        // require a valid candidate
        require(_id > 0 && _id <= totalCandidates);
        // record that voter has voted
        voters[msg.sender] = true;
        // update candidate vote Count
        candidates[_id].voteCount++;
        // trigger voted event
        emit Voted(_id);
    }

}
