pragma solidity >=0.4.25 <0.7.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Election.sol";

contract TestElection {

  function testInitialCandidateUsingDeployedContract() public {
    Election election = Election(DeployedAddresses.Election());
    uint expected = 1;
    Assert.equal(election.totalCandidates(), expected, "Election should have 1 Candidate initially");
  }

  function testInitialCandidateWithNewElection() public {
    Election election = new Election();
    uint expected = 1;
    Assert.equal(election.totalCandidates(), expected, "Election should have 1 Candidate initially");
  }

}
