const Election = artifacts.require("Election");

contract('Election', (accounts) => {
  it('should have 1 candidate in the election', async () => {
    const electionInstance = await Election.deployed();
    const candidatesCount = await electionInstance.totalCandidates();
    assert.equal(candidatesCount.valueOf(), 1, "There wasn't 1 candidate in the election");
  });
  it("initializes the candidate with the correct values", async () => {
    const electionInstance = await Election.deployed();
    const candidate = await electionInstance.candidates(1);
    assert.equal(candidate[0], 1, "doesn't contain the correct id");
    assert.equal(candidate[1], "Dummy", "doesn't contain the correct name");
    assert.equal(candidate[2], 0, "doesn't contain the correct votes count");
  });
  it("allows a voter to cast a vote", async () => {
    const electionInstance = await Election.deployed(), candidateId = 1; 
    const receipt = await electionInstance.vote(candidateId, { from: accounts[0] });
    assert.equal(receipt.logs.length, 1, "an event was not triggered");
    assert.equal(receipt.logs[0].event, "Voted", "the event type is not correct");
    assert.equal(receipt.logs[0].args.id.toNumber(), candidateId, "the candidate id is not correct");
    const voted = await electionInstance.voters(accounts[0]);
    assert(voted, "the voter was not marked as voted");
    const candidate = await electionInstance.candidates(candidateId);
    const candidateVoteCount = candidate[2];
    assert.equal(candidateVoteCount, 1, "candidate's vote count not incremented");
  });
  it("throws an exception for invalid candidates", async () => {
    const electionInstance = await Election.deployed();
    let candidateId = 99; 
    try {
      const receipt = await electionInstance.vote(candidateId, { from: accounts[0] });
      assert.fail(receipt);
    } catch(error) {
      assert(error.message.indexOf('revert') >= 0, "error message must contain revert");
      candidateId = 1;
      const candidate = await electionInstance.candidates(candidateId);
      assert.equal(candidate[2], 1, "candidate did not receive any votes");
    }
  });
  it("throws an exception for double voting", async () => {
    const electionInstance = await Election.deployed();
    let candidateId = 1, candidate = null; 
    try {
      await electionInstance.vote(candidateId, { from: accounts[0] });
      candidate = await electionInstance.candidates(candidateId);
      assert.equal(candidate[2], 1, "candidate did not receive any votes");
      const receipt = await electionInstance.vote(candidateId, { from: accounts[0] });
      assert.fail(receipt);
    } catch(error) {
      assert(error.message.indexOf('revert') >= 0, "error message must contain revert");
      candidate = await electionInstance.candidates(candidateId);
      assert.equal(candidate[2], 1, "candidate did not receive any votes");
    }
  });
});
